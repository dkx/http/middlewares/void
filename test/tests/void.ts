import {Response, testMiddleware} from '@dkx/http-server';
import {expect} from 'chai';
import {voidMiddleware} from '../../lib';


describe('#Middlewares/voidMiddleware', () => {

	it('should do nothing', async () => {
		const res = await testMiddleware(voidMiddleware());
		expect(res).to.be.an.instanceOf(Response);
	});

});
