import {Request, Response, NextMiddlewareFunction, Middleware} from '@dkx/http-server';


export function voidMiddleware(): Middleware
{
	return (req: Request, res: Response, next: NextMiddlewareFunction): Promise<Response> =>
	{
		return next(res);
	}
}
