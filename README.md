# DKX/Http/Middlewares/Void

Middleware for [@dkx/http-server](https://gitlab.com/dkx/http/server) which does nothing at all.

## Installation

```bash
$ npm install --save @dkx/http-middleware-void
```

or with yarn

```bash
$ yarn add @dkx/http-middleware-void
```

## Usage

```js
const {Server} = require('@dkx/http-server');
const {voidMiddleware} = require('@dkx/http-middleware-void');

const app = new Server;

app.use(voidMiddleware());
```
